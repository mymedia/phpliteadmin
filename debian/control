Source: phpliteadmin
Section: web
Priority: optional
Maintainer: Nicholas Guriev <guriev-ns@ya.ru>
Build-Depends: debhelper-compat (= 13), dh-apache2
Build-Depends-Indep: php-cli
Standards-Version: 4.6.0
Rules-Requires-Root: binary-targets
Homepage: https://www.phpliteadmin.org/
Vcs-Git: https://salsa.debian.org/mymedia/phpliteadmin.git
Vcs-Browser: https://salsa.debian.org/mymedia/phpliteadmin

Package: phpliteadmin
Architecture: all
Depends:
 libapache2-mod-php | php-cgi | php-fpm,
 libjs-codemirror (>= 5.25.0),
 php-mbstring,
 php-sqlite3,
 ${misc:Depends},
Recommends: ${misc:Recommends}
Suggests: www-browser
Description: web-based SQLite database admin tool
 phpLiteAdmin is a web-based SQLite database admin tool written in PHP with
 support for SQLite3 and SQLite2 (not in Debian). Following in the spirit of the
 flat-file system used by SQLite, phpLiteAdmin consists of a single source file,
 phpliteadmin.php, that is dropped into a directory on a server and then visited
 in a browser. The available operations, feature set, interface, and user
 experience is comparable to that of phpMyAdmin.
 .
 In this package, you will find phpliteadmin.php already configured to work with
 Apache2. This package provides the main features of the script.

Package: phpliteadmin-themes
Architecture: all
Depends: phpliteadmin, ${misc:Depends}
Description: web-based SQLite database admin tool - themes
 phpLiteAdmin is a web-based SQLite database admin tool written in PHP with
 support for SQLite3 and SQLite2 (not in Debian). Following in the spirit of the
 flat-file system used by SQLite, phpLiteAdmin consists of a single source file,
 phpliteadmin.php, that is dropped into a directory on a server and then visited
 in a browser. The available operations, feature set, interface, and user
 experience is comparable to that of phpMyAdmin.
 .
 This package contains styles of themes for phpLiteAdmin.
